from prometheus_client import start_http_server, REGISTRY
import os
import time
from haproxy_exporter import haproxy

if __name__ == "__main__":
    p = os.environ.get('port_list')
    h = os.environ.get('host')
    ep = os.environ.get('ep')
    start_http_server(int(ep))
    REGISTRY.register(haproxy.HaproxyComp(h, p))
    while True: time.sleep(1)