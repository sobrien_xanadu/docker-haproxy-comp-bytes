from prometheus_client import Metric
import requests
import csv
import json


def haproxy_stats_to_json(haproxy_url):
    with requests.Session() as s:
        haproxy_csv = s.get(haproxy_url) # haproxy01 c5
        decoded_haproxy_csv = haproxy_csv.content.decode('utf-8')
        stats = csv.reader(decoded_haproxy_csv.splitlines(), delimiter=',')
        stats_list = list(stats)
        heading_list = stats_list[0]
        metrics_dict = {}
        for mlist in stats_list:
            curr_dict = dict(zip(heading_list, mlist))
            key = curr_dict['# pxname']
            metrics_dict.update({key: curr_dict})
        del metrics_dict['# pxname']
        r = json.dumps(metrics_dict)
        return json.loads(r)


class HaproxyComp(object):
    def __init__(self, endpoint, ports):
        self._endpoint = endpoint
        self._ports = ports

    def collect(self):
        port_list = self._ports.split()
        # :8001/stats;csv
        for port in port_list:
            endpoint_url = "http://" + self._endpoint + ":" + str(port) + "/stats;csv"
            json = haproxy_stats_to_json(endpoint_url)
            hostname = self._endpoint
            # BACKEND details
            comp_bytes_in = Metric('haproxy_backend_comp_bytes_in', 'compressed bytes in', 'summary')
            comp_bytes_out = Metric('haproxy_backend_comp_bytes_out', 'compressed bytes out', 'summary')
            for i in json:
                if 'BACKEND' in json[i]['svname']:
                    comp_bytes_in.add_sample('haproxy_backend_comp_bytes_in_count',value=float(json[i]['comp_in'])
                                                   ,labels={'host':hostname, 'pid':json[i]['pid'], 'job':i})
                    comp_bytes_out.add_sample('haproxy_backend_comp_bytes_out_count', value=float(json[i]['comp_out']),
                                              labels={'host': hostname, 'pid':json[i]['pid'],  'job': i})
            yield comp_bytes_in
            yield comp_bytes_out